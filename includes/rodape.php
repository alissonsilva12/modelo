<!-- Footer -->
	    <div class="footer">
	    	<div class="container">
		    	<div class="row">
		    		<div class="col-footer col-md-2 col-xs-6">
		    		<br><br>
		    		<div class="portfolio-item">
							<div class="portfolio-image">
								<a target="blank" href="http://portal.detran.ce.gov.br/index.php"><img src="img/logomarca.png.jpeg"></a>
							</div>
						</div>
		    		</div>
		    		<div class="col-footer col-md-3 col-xs-6">
		    			<h3>Links Úteis do DETRAN-CE</h3>
		    			<ul class="no-list-style footer-navigate-section">
		    				<li><a target="blank" href="http://portal.detran.ce.gov.br/index.php/emissao-de-taxas">Licenciamento</a></li>
		    				<li><a target="blank" href=https://www.sefaz.ce.gov.br/content/aplicacao/internet/servicos_online/ipva/aplic/default_ano.asp">IPVA</a></li>
		    				<li><a target="blank" href="http://www.denatran.gov.br/">Denatran</a></li>
		    				<li><a target="blank" href="http://www.fortaleza.ce.gov.br/tags/amc">AMC</a></li>
		    				<li><a target="blank" href="http://portal.detran.ce.gov.br/index.php/consulta-de-habilitacao">Pontuação na CNH</a></li>
		    				<li><a target="blank" href="http://portal.detran.ce.gov.br/index.php/carteira-de-motorista-popular">Carteira Popular</a></li>
		    			</ul>
		    		</div>
		    		
		    		<div class="col-footer col-md-4 col-xs-6">
		    			<h3>Informações Gerais</h3>
		    			<p class="contact-us-details">
	        				<b>Endereço:</b> Rua Miguel Aragão, 1160 - Mondubim - Fortaleza- CE.<br/>
	        				<b>Fone:</b> (85) 8849-9955<br/>
	        				<b>Email:</b> <a href="mailto:getintoutch@yourcompanydomain.com"> teste@gmail.com</a>
	        			</p>
		    		</div>
		    		<div class="col-footer col-md-2 col-xs-6">
		    			<h3>Redes Sociais</h3>
		    			<ul class="footer-stay-connected no-list-style">
		    				<li><a href="#" class="facebook"></a></li>
		    				<li><a href="#" class="twitter"></a></li>
		    				<li><a href="#" class="googleplus"></a></li>
		    			</ul>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-md-12">
		    			<div class="footer-copyright">&copy; 2015 Reobote Soluções Web. All rights reserved.</div>
		    		</div>
		    	</div>
		    </div>
	    </div>