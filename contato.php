<?php 

include 'includes/menu.php';

?>
		<!-- Título da página -->
		<div class="section section-breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h1>Contato</h1>
					</div>
				</div>
			</div>
		</div>
        
        <div class="section">
	    	<div class="container">
	        	<div class="row">
	        		<div class="col-sm-7">
	        		<!-- Map -->
	        			<div id="contact-us-map">
	        			</div>
	        			<!-- End Map -->
	        			<!-- Contact Info -->
	        			<p class="contact-us-details">
	        				<b>Endereço:</b> Rua Miguel Aragão,1160, Mondubim, Fortaleza, Ceará<br/>
	        				<b>Fone:</b> (85) 8849-9955<br/>
	        				<b>Email:</b> <a href="mailto:getintoutch@yourcompanydomain.com">teste@gmail.com</a>
	        			</p>
	        			<!-- End Contact Info -->
	        		</div>
	        		<div class="col-sm-5">
	        			<!-- Contact Form -->
	        			<h3>Envie sua Mensagem</h3>
	        			<div class="contact-form-wrapper">
		        			<form class="form-horizontal" role="form">
		        				 <div class="form-group">
		        				 	<label for="Name" class="col-sm-3 control-label"><b>Nome:</b></label>
		        				 	<div class="col-sm-9">
										<input class="form-control" id="Name" type="text" required placeholder="">
									</div>
								</div>
								<div class="form-group">
									<label for="contact-email" class="col-sm-3 control-label"><b>Email:</b></label>
									<div class="col-sm-9">
										<input class="form-control" id="contact-email" type="email" required  placeholder="">
									</div>
								</div>
								<div class="form-group">
									<label for="contact-message" class="col-sm-3 control-label"><b>Assunto:</b></label>
									<div class="col-sm-9">
										<select class="form-control" id="prependedInput">
											<option>Dúvidas</option>
											<option>Reclamações</option>
											<option>Sugestões</option>
											<option>Outros</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="contact-message" class="col-sm-3 control-label"><b>Mensagem:</b></label>
									<div class="col-sm-9">
										<textarea class="form-control" rows="5" id="contact-message"></textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-12">
										<button type="submit" class="btn pull-right">Enviar</button>
									</div>
								</div>
		        			</form>
		        		</div>
		        		<!-- End Contact Info -->
	        		</div>
	        	</div>
	    	</div>
	    </div>
	    <?php 

include 'includes/rodape.php';

?> 